var Promise = require('promise');
var config = require('./config/config.json').recommendation
var request = require("request");

var closure = {
    getRecommendations: function (experimentId, userId, questions, recommender) {
        return new Promise(function (fullfil, reject) {
            var rec = {
                experimentId: experimentId,
                userId: userId,
                questions: questions,
                recommender: recommender
            }
            request.post({
                headers: {'content-type': 'application/json'},
                url: config.recommendationUrl,
                json: rec
            }, function (error, response, body) {
                fullfil(body.recommendations);
            })
        })
    },
    getRecommendersList: function () {
        return new Promise(function (fullfil, reject) {
            request.get({
                url: config.recommendersListUrl
            }, function (error, response, body) {
                fullfil(JSON.parse(body));
            })
        })
    }
}

module.exports = closure;