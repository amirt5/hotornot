"use strict";

module.exports = function(sequelize, DataTypes) {
    var AccessToken = sequelize.define("AccessToken", {
        access_token: {
            type: DataTypes.STRING,
            primaryKey: true
        }
    }, {
        classMethods: {
            associate: function(models) {
                AccessToken.belongsTo(models.User);
            }
        }
    });

    return AccessToken;
};