"use strict";

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define("User", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                User.hasMany(models.AccessToken);
                User.belongsToMany(models.ExperimentGroup, {through: 'ExperimentGroupUser'})
            }
        }
    });

    return User;
};