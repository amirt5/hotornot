"use strict";

module.exports = function (sequelize, DataTypes) {
    var Experiment = sequelize.define("Experiment", {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        CourseId: DataTypes.INTEGER
    }, {
        classMethods: {
            associate: function (models) {
                Experiment.hasMany(models.ExperimentGroup)
            }
        }
    });

    return Experiment;
};