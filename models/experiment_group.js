"use strict";

module.exports = function (sequelize, DataTypes) {
    var ExperimentGroup = sequelize.define("ExperimentGroup", {
            recommender: DataTypes.STRING
        },
        {
            classMethods: {
                associate: function (models) {
                    ExperimentGroup.belongsTo(models.Experiment)
                    ExperimentGroup.belongsToMany(models.User, {through: 'ExperimentGroupUser'})
                }
            }
        });

    return ExperimentGroup;
};