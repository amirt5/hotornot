var db = require('../db')
var express = require('express');
var router = express.Router();
var recommendations = require("../recommendation");
var canvas = require("../canvas");

/* GET users listing. */
router.get('/', function (req, res) {
    getExperimentsForUser(req.cookies.auth)
        .then(function (experiments) {
            res.json(experiments);
        })
});

function getExperimentsForUser(token) {
    return db.getUserByToken(token)
        .then(function (user) {
            return db.getExperimentsForUser(user['_id']);
        })
}

module.exports = router;
