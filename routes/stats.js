var db = require('../db')
var canvas = require('../canvas')
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    var experimentId = req.param("experimentId");
    getExperimentStats(experimentId)
        .then(function (stats) {
            res.json(stats);
        })
});

function averageGrades(usersGrades) {
    return usersGrades.map(function (x) {
        return x.grades.map(function (x) {
            return x.grade;
        })
    }).map(function (x) {
        var numOfGrades = x.length;
        var sumGrades = x.reduce(function(a, b) {
            return (a + b);
        }, 0);
        if (numOfGrades > 0) {
            return sumGrades / numOfGrades;
        } else {
            return 0;
        }
    });
}

function getExperimentStats(experimentId) {
    return db.getExperiment(experimentId)
        .then(function (experiment) {
            return canvas.getQuizSubmissions(experiment.courseId, experiment.quizId)
                .then(function (submissions) {
                    return {
                        stats: submissions.map(function (submission) {
                            return {
                                userId: submission.user_id,
                                grade: submission.score
                            }
                        })
                    }
                })
        })
}



module.exports = router;
