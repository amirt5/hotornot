/**
 * Created by naorguetta on 11/4/14.
 */
var db = require('../db')
var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var canvas = require("../canvas");

router.post('/', function (req, res, next) {
    var token = req.cookies.auth;
    var body = req.body;
    var chosenQuestionId = body.chosen_question_id;
    var unChosenQuestionId = body.unchosen_question_id;
    var answer = body.answer;
    var experimentId = body.experimentId;
    var time = body.time;

    insertSubmission(token, experimentId, chosenQuestionId, answer)
        .then(function () {
            res.send('ok');
        });
});

function insertSubmission(token, experimentId, chosenQuestionId, answer) {
    return db.getUserByToken(token)
        .then(function (user) {
            if (user != null) {
                return user['id'];
            }
        })
        .then(function (userId) {
            return db.getExperimentForUser(experimentId, userId)
                .then(function (experiment) {
                    var courseId = experiment.courseId;
                    var quizId = experiment.quizId;
                    return canvas.answerQuizSubmissionQuestion(courseId, quizId, chosenQuestionId, answer, token)
                })
        })
}


module.exports = router;