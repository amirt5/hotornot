var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var request = require("request");
var db = require("../db");
var Promise = require('promise');
var canvas = require("../canvas");
var recommendations = require("../recommendation");


router.get('/', function (req, res) {
    var token = req.cookies.auth;
    var experimentId = req.param("experimentId");
    getHotOrNot(token, experimentId)
        .then(function (hotOrNot) {
            res.json(hotOrNot)
        });
});


function getHotOrNot(token, experimentId) {
    return db.getUserByToken(token)
        .then(function (user) {
            if (user != null) {
                return user['id'];
            }
        })
        .then(function (userId) {
            return db.getRecommenderForUser(experimentId, userId)
                .then(function (recommender) {
                    console.log("recommender is " + recommender);
                    return db.getExperimentForUser(experimentId, userId)
                        .then(function (experiment) {
                            var courseId = experiment.courseId;
                            var quizId = experiment.quizId;
                            var numQuestions = experiment.numQuestions;
                            return getHotOrNotJson(experimentId, userId, token, courseId, quizId, numQuestions, recommender);
                        })
                })
        })
}

function getHotOrNotJson(experimentId, userId, token, courseId, quizId, numQuestions, recommender) {
    return getCourseQuizSubmission(courseId, quizId, token)
        .then(function (submission) {
            var allQuestions = submission.questions;
            var numQuestionsToAnswer = submission.numQuestionsToAnswer;
            var recommendFrom = filterQuestions(allQuestions, numQuestionsToAnswer);
            return recommendQuestions(experimentId, userId, recommendFrom, recommender)
                .then(function (recommendedQuestions) {
                    return {
                        questionNumber: numQuestionsToAnswer - recommendedQuestions.length,
                        totalQuestions: numQuestionsToAnswer,
                        questions: recommendedQuestions.slice(0, numQuestions),
                        completed: submission.completed
                    }
                })
        })
}

function filterQuestions(questions, n) {
    var ans = [];
    for (var i = 0; i < questions.length; i++) {
        if (questions[i].position != i + 1) {
            ans.push(questions[i]);
        }
    }
    var positionsAdded = ans.map(function (x) { return x.position });
    for (var position = 1; position <= n && ans.length < n; position++) {
        if (positionsAdded.indexOf(position) < 0) {
            ans.push(questions[position - 1]);
        }
    }
    return ans.filter(function (x) { return x.answer == null });
}

function getCourseQuizSubmission(courseId, quizId, token) {
    return canvas.createOrGetQuizSubmission(courseId, quizId, token)
        .then(function (submission) {
            return canvas.getSubmissionQuestions(submission.id, token)
                .then(function (submissionQuestions) {
                    return {
                        questions: submissionQuestions,
                        completed: submission.workflow_state == 'complete',
                        numQuestionsToAnswer: submission.quiz_points_possible
                    }
                })
        })
}

function recommendQuestions(experimentId, userId, canvasQuestions, recommender) {
    var requestQuestions = canvasQuestions.map(function (q) {
        return {
            id: q.assessment_question_id,
            subjectId: canvas.questionSubjectId(q.question_name),
            level: canvas.questionLevel(q.question_name)
        };
    });
    return recommendations.getRecommendations(experimentId, userId, requestQuestions, recommender)
        .then(function (recommendedQuestions) {
            var originalOrderedIds = canvasQuestions.map(function (q) { return q.assessment_question_id });
            var ans = [];
            for (var i = 0; i < recommendedQuestions.length; i++) {
                var oldPosition = originalOrderedIds.indexOf(recommendedQuestions[i])
                ans.push(canvasQuestions[oldPosition]);
            }
            return ans;
        });
}

function removeAnsweredQuestions(canvasQuestions, questionsIdsToRemove) {
    var ans = [];
    canvasQuestions.forEach(function (element) {
        if (questionsIdsToRemove.indexOf(element.id) < 0) {
            ans.push(element);
        }
    })
    return ans;
}


module.exports = router;