var db = require('../db')
var express = require('express');
var router = express.Router();
var recommendations = require("../recommendation");
var canvas = require("../canvas");

/* GET users listing. */
router.get('/', function (req, res) {
    getExperimentData(req.cookies.auth)
        .then(function (experimentData) {
            res.json(experimentData);
        })
});

function getExperimentData(token) {
    return recommendations.getRecommendersList()
        .then(function (recommenders) {
            return canvas.getCourses()
                .then(function (courses) {
                    return {
                        recommenders: recommenders.recommenders,
                        courses: courses
                    }
                })
        })
}

module.exports = router;
