/**
 * Created by naorguetta on 11/4/14.
 */
var db = require('../db');
var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var canvas = require("../canvas");


router.get('/', function (req, res) {
    if (req.param('error')) {
        res.redirect('html/index.html');
    } else {
        canvas.oAuth(req.query.code)
            .then(function (response) {
                console.log(response);

                var jsonAuth = JSON.parse(response);

                var user = jsonAuth['user'];
                var userId = user['id'];
                var userName = user['name'];
                var access_token = jsonAuth['access_token'];

                canvas.isAdmin(userId)
                    .then(function (isAdmin) {
                        db.insertUser(userId, userName, isAdmin, access_token);
                        res.header('Set-Cookie', "auth=" + jsonAuth["access_token"]);
                        res.redirect('html/index.html');
                    })
            });
    }
});


module.exports = router;