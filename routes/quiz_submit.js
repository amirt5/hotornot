var db = require('../db');
var canvas = require('../canvas');
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    var token = req.cookies.auth;
    var experimentId = req.param("experimentId");
    db.getUserByToken(token)
        .then(function (user) {
            if (user != null) {
                var userId = user['id'];
                return completeQuizSubmission(token, userId, experimentId);
            }
        })
        .then(function () {
            res.send('ok');
        })
});

function completeQuizSubmission(token, userId, experimentId) {
    return db.getExperimentForUser(experimentId, userId)
        .then(function (experiment) {
            var courseId = experiment.courseId;
            var quizId = experiment.quizId;

            return canvas.completeQuizSubmission(courseId, quizId, token);
        })
}


module.exports = router;
