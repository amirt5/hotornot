var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var config = require('../config/config.json')

router.get('/', function (req, res) {
    var redirect = config.canvas.url + "/login/oauth2/auth?" +
        "client_id=" + config.canvas.clientId + "&response_type=code&" +
        "redirect_uri=" + config.hotOrNot.url + "/oauth2response";

    console.log(redirect);
    res.redirect(redirect);
});

module.exports = router;