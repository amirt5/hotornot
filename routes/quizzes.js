var express = require('express');
var router = express.Router();
var debug = require('debug')('HotOrNotServer');
var request = require("request");
var db = require("../db");
var Promise = require('promise');
var canvas = require("../canvas");
var recommendations = require("../recommendation");


router.get('/', function (req, res) {
    var token = req.cookies.auth;
    var experimentId = req.param("experimentId");
    getHotOrNot(token, experimentId)
        .then(function (hotOrNot) {
            res.json(hotOrNot)
        });
})

function getHotOrNot(token, experimentId) {
    return db.getUserByToken(token)
        .then(function (user) {
            if (user != null) {
                return user['id'];
            }
        })
        .then(function (userId) {
            return db.getRecommenderForUser(experimentId, userId)
                .then(function (recommender) {
                    console.log("recommender is " + recommender);
                    return db.getExperimentForUser(experimentId, userId)
                        .then(function (experiment) {
                            var courseId = experiment.courseId;
                            var quizId = experiment.quizId;
                            var numQuestions = experiment.numQuestions;
                            return getHotOrNotJson(experimentId, userId, token, courseId, quizId, numQuestions, recommender);
                        })
                })
        }
    )
}

function getHotOrNotJson(experimentId, userId, token, courseId, quizId, numQuestions, recommender) {
    console.log(courseId + " " + quizId)
    return canvas.getter('getCourseQuizSubmission', {course_id: courseId, quiz_id: quizId}, token)
        .then(function (cnvsQuestions) {
            console.log("canvas questions are " + cnvsQuestions);
            return db.getQuestionsAnswered(experimentId, userId)
                .then(function (ansrdQuestions) {
                    console.log(ansrdQuestions);
                    var questionsToRecommend = removeAnsweredQuestions(cnvsQuestions, ansrdQuestions);
                    return sortByRecommendation(userId, questionsToRecommend, recommender)
                        .then(function (sortedQuestions) {

                            return {
                                "questionNumber": ansrdQuestions.length + 1,
                                "totalQuestions": cnvsQuestions.length,
                                "questions": sortedQuestions.slice(0, numQuestions)
                            }
                        })
                })
        })
}

function sortByRecommendation(userId, canvasQuestions, recommender) {
    var questionIds = canvasQuestions.map(function (q) {
        return q.id;
    });
    return recommendations.getRecommendations(userId, questionIds, recommender)
        .then(function (orderedIds) {
            var ans = [];
            for (var i = 0; i < orderedIds.length; i++) {
                var oldPosition = questionIds.indexOf(orderedIds[i])
                ans[i] = canvasQuestions[oldPosition];
            }
            return ans;
        });
}

function removeAnsweredQuestions(canvasQuestions, questionsIdsToRemove) {
    var ans = [];
    canvasQuestions.forEach(function (element) {
        if (questionsIdsToRemove.indexOf(element.id) < 0) {
            ans.push(element);
        }
    })
    return ans;
}


module.exports = router;