var HotOrNot = angular.module("HotOrNot", ['ngAnimate', 'ui.bootstrap', 'ngSanitize', 'checklist-model', 'ui.router', 'pascalprecht.translate', 'ngCookies', 'angularjs-dropdown-multiselect', 'ui.slider', 'chart.js','cgNotify']);
// split one and two question in two diffrent variable that delete each other so I can predict which one is on
HotOrNot.controller("QuestionDisplay", ['$scope', '$http', '$state', '$stateParams','notify', function ($scope, $http, $state, $stateParams,notify) {
    var debug = false;
    var startTime;
    $scope.questionAnswered = 0;
    $scope.totalQuestion = 13;
    $scope.answerFeedback = 'טוען את השאלות';
    $scope.secondAttempt = false;
    var experimentId = $stateParams.experimentId;
    var savedAnswer;
    var savedQuestion;
    var savedUnSelectedQuestion;


    getNewQuestions();





    $scope.submit = function (question) {
        var unSelectedQuestion = 0;
        for (var i = 0; i < $scope.questions.length ; i++) {
            if ($scope.questions[i].id !== question.id) {
                unSelectedQuestion = $scope.questions[i];
            }
            $scope.questions[i].disableAns = true;
        }
        $scope.answerFeedback = "בודק תשובה אנא המתן";
        $scope.secondAttempt = false;
        savedAnswer = question.studentAnswer; // can be removed
        savedQuestion = question;
        savedUnSelectedQuestion = unSelectedQuestion;
        $http.post('/submissions_attempts', {
            experimentId: experimentId,
            chosen_question_id: savedQuestion.id,
            chosen_assessment_question_id: savedQuestion.assessment_question_id,
            unchosen_question_id: savedUnSelectedQuestion,
            unchosen_assessment_question_id: savedUnSelectedQuestion.assessment_question_id,
            time: new Date().getTime() - startTime,
            answer: savedAnswer
        }).
            success(function (data, status, headers, config) {
                if(data.feedback) {
                    notify({ messageTemplate:'<h4>!!כל הכבוד, תשובה נכונה</h4>', classes:'alert-success',duration:'3000' } );
                    $scope.answerFeedback = "כל הכבוד, תשובה נכונה!!";
                    $scope.sendFinalAnswer();
                } else {
                    for (var i = 0; i < $scope.questions.length ; i++) {
                        $scope.questions[i].disableAns = false;
                    }
                    notify({ messageTemplate:'<h4>תשובה לא נכונה. באפשרותך לנסות שנית או ללחוץ על כפתור ״דלג״</h4>', classes:'alert-warning',duration:'3000' } );
                    $scope.answerFeedback = 'תשובה לא נכונה. באפשרותך לנסות שנית או ללחוץ על';
                    $scope.secondAttempt = true;

                }
            }).
            error(function (data, status, headers, config) {
                if (debug) {
                    $scope.answerFeedback = "תשובה נכונה! טוען שאלות חדשות...";
                    $scope.sendFinalAnswer();
                } else {
                    $scope.answerFeedback = "שגיאה אנא קרא למישהו מוסמך";
                }
            });

    };


    $scope.sendFinalAnswer = function() {
        $scope.secondAttempt = false;
        $http.post('/submissions_canvas', {
            experimentId: experimentId,
            chosen_question_id: savedQuestion.id,
            chosen_assessment_question_id: savedQuestion.assessment_question_id,
            unchosen_question_id: savedUnSelectedQuestion,
            unchosen_assessment_question_id: savedUnSelectedQuestion.assessment_question_id,
            time: new Date().getTime() - startTime,
            answer: savedAnswer
        }).
            success(function (data, status, headers, config) {
                getNewQuestions();
            }).
            error(function (data, status, headers, config) {
                if (debug) {
                    getNewQuestions();
                } else {
                    $scope.answerFeedback = "שגיאה אנא קרא למישהו מוסמך";
                }
            });
    }


    $scope.ResetQuestions = function () {
        $http.get('/resubmit').success(function (data) {
            $state.reload();
        });

    }


    function getNewQuestions() {

        $scope.questions = null;
        $http.get(debug ? '../Questions.json' : '/quizzes_canvas?experimentId=' + experimentId).success(function (data) {
            if (data.questions.length > 0 && !data.completed) {

                $scope.questions = data.questions;
                $scope.questionAnswered = data.questionNumber;
                $scope.totalQuestion = data.totalQuestions;
                $scope.answer = null;
                startTime = new Date().getTime();

                $scope.questions.forEach(function(question) {
                    question.question_name = parseInt(question.question_name.split(":",1));
                    switch  (question.question_name) {
                        case 1:
                            question.question_name = "שלמים";
                            break;
                        case 2:
                            question.question_name = "שברים";
                            break;
                        case 3:
                            question.question_name = "גאומטריה";
                            break;
                        case 4:
                            question.question_name = "בעיה מילולית";
                            break;
                    }
                });

                if($scope.questions.length > 1) {
                    $scope.answerFeedback =  "אנא בחר אחת מן השאלות הבאות וענה עליה";
                    for(var i=0 ; i < $scope.questions.length ; i ++) {
                        $scope.questions[i].questionTitle = 'שאלה לבחירה';
                    }
                } else {
                    $scope.answerFeedback =  "ענה על השאלה";
                    $scope.questions[0].questionTitle = '';
                }
            } else {
                if (!data.completed) {
                    $http.get('/quiz_submit?experimentId=' + experimentId).success(function() {
                        getNewQuestions();
                    });
                } else {
                    $scope.answerFeedback = "סיימת את התרגול, תודה!";
                    notify({ messageTemplate:'<h4>! סיימת את התרגול, תודה</h4>', classes:'alert-success',duration:'3000' } );
                    $scope.questionAnswered = $scope.totalQuestion;
                }
            }
        });
    }
}]);




HotOrNot.controller('TranslateController', function ($translate, $scope) {
    $scope.changeLanguage = function () {
        if ($translate.use() == 'en') {
            $translate.use('he');
        }
        else {
            $translate.use('en');
        }

    };
});

HotOrNot.controller('CreateExp', function ($scope, $http, $state) {
    //$scope.groups = [];

    $scope.numberOfQuestions = 2;
    $scope.selectedCourse = [];
    $scope.selectedQuiz = [];
    $scope.quizzes = [];
    $scope.selectedAlgorithms = [];
    $scope.sliderBar = 0;

    $scope.expName = null;
    $scope.expDes = null;

    $scope.settingsQuizzes = {
        scrollableHeight: '300px',
        scrollable: true,
        externalIdProp: '',
        displayProp: 'title',
        idProp: 'title',
        enableSearch: true,
        selectionLimit: 1,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    $scope.settingsCourses = {
        scrollableHeight: '300px',
        scrollable: true,
        externalIdProp: '',
        displayProp: "name",
        enableSearch: true,
        selectionLimit: 1,
        smartButtonMaxItems: 1,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };
    $scope.settingsAlgorithms = {
        externalIdProp: 'recommender',
        idProp: 'name',
        displayProp: "name",
        selectionLimit: 2,
        smartButtonMaxItems: 2,
        showUncheckAll: false,
        smartButtonTextConverter: function (itemText, originalItem) {
            return itemText;
        }
    };

    function getData() {

        //$http.get('../createExp.json').success(function (data) {
        $http.get('/experiments_data').success(function (data) {
            $scope.courses = data.courses;
            $scope.algo = data.recommenders;


        });
    }

    getData();


    //$scope.selectedCourse.id = null;
    $scope.$watch('selectedCourse.id', function (newVal) {
        if (newVal != null) {
            $http.get('/experiments_quizzes?courseId=' + newVal)
                //$http.get('../Quizzes.json')
                .success(function (data) {
                    $scope.selectedQuiz = [];
                    $scope.quizzes = data.quizzes;
                })

        }

    })
    $scope.submit = function () {

        if ($scope.selectedAlgorithms.length == 1) {
            $scope.selectedAlgorithms[0].percentage = 100;
        } else {
            $scope.selectedAlgorithms[0].percentage = $scope.sliderBar;
            $scope.selectedAlgorithms[1].percentage = 100 - $scope.sliderBar;
        }

        //console.log($scope.selectedQuiz)

        $http.post('/experiment_submit', {
            name: $scope.expName,
            description: $scope.expDes,
            courseId: $scope.selectedCourse.id,
            quizId: $scope.selectedQuiz.id,
            groups: $scope.selectedAlgorithms,
            questionsNumber: $scope.numberOfQuestions
        })
            .success(function (data, status, headers, config) {
                $state.go('Home');
            })
            .error(function (data, status, headers, config) {

            });

    }
});








