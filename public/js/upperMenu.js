/**
 * Created by Amir on 30/06/2015.
 */
HotOrNot.controller("UpperMenu", ['$scope', '$http', '$cookies', function ($scope, $http, $cookies) {

    $http.get('/users').success(function (data) {
        $scope.userName = data;
    });

    $http.get('/experiments_stats').success(function (data) {
        $scope.statisticsData = data.experiments_stats;
    });


    $http.get('/experiments_user').success(function (data) {
        //$http.get('../MyExp.json').success(function (data) {

        $scope.experiments = data.experiments;
    });

    $scope.Logout = function () {
        $cookies.remove('auth');
    }
}]);