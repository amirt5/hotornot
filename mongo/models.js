var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require("../config/config.json").db.mongo;

mongoose.connect(config.url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log("connection opened")
});

var models = {
    User: mongoose.model('User', new Schema({
        _id: Number,
        name: String,
        isAdmin: Boolean,
        tokens: [String]
    })),
    Experiment: mongoose.model('Experiment', new Schema({
        name: String,
        description: String,
        courseId: Number,
        quizId: Number,
        numQuestions: Number,
        groups: [{
            recommender: String,
            users: [{type: Number, ref: 'User'}]
        }]
    })),
    Submission: mongoose.model('Submission', new Schema({
        userId: {type: Number, ref: 'User'},
        experimentId: {type: Schema.Types.ObjectId, ref: 'Experiment'},
        questions: [Number]
    }))
};


module.exports = models;
